A Powerline style prompt for your shell
=======================================

A [Powerline](https://github.com/Lokaltog/vim-powerline) like prompt for Bash, ZSH and Fish:

![MacVim+Solarized+Powerline+CtrlP](https://raw.github.com/milkbikis/dotfiles-mac/master/bash-powerline-screenshot.png)

*  Shows some important details about the git/svn/hg/fossil branch:
    *  Displays the current branch which changes background color when the branch is dirty
    *  A '+' appears when untracked files are present
    *  When the local branch differs from the remote, the difference in number of commits is shown along with '⇡' or '⇣' indicating whether a git push or pull is pending
*  Changes color if the last command exited with a failure code
*  If you're too deep into a directory tree, shortens the displayed path with an ellipsis
*  Shows the current Python [virtualenv](http://www.virtualenv.org/) environment
*  It's easy to customize and extend. See below for details.

Basically it's a port of [Powerline Shell](https://github.com/milkbikis/powerline-shell) but instead of using Python to generate nice prompt, we're using Go, so we're getting like 10x performance gain.

# Setup

```
sudo apt-get install mercurial
```

Then:

1. Download Go compiler
2. `tar -zxvf go1.3.1.linux-amd64.tar.gz`
3. Add go bin path to your PATH, ex: `export PATH=$PATH:/home/kkszysiu/go/bin/`
4. Set a path to GOROOT - so directory where you have your go compiler, ex. `export GOPATH=/home/kkszysiu/go/`
5. Go to the powerline-shell-go directory
6. Type: `make restore`
7. Type: `make`
8. After that powerline-shell-go binary should be in `dist` path
9. Open `~/.bashrc`
10. Add:

### Bash:
Add the following to your `.bashrc`:
```bash
        function _update_ps1() {
           export PS1="$(/home/kkszysiu/powerline-shell-go/dist/powerline-shell-go $? 2> /dev/null)"
        }

        export PROMPT_COMMAND="_update_ps1; $PROMPT_COMMAND"
```
### ZSH:
Add the following to your `.zshrc`:
```zsh
        function powerline_precmd() {
          export PS1="$(/home/kkszysiu/powerline-shell-go/dist/powerline-shell-go $? --shell zsh 2> /dev/null)"
        }

        function install_powerline_precmd() {
          for s in "${precmd_functions[@]}"; do
            if [ "$s" = "powerline_precmd" ]; then
              return
            fi
          done
          precmd_functions+=(powerline_precmd)
        }

        install_powerline_precmd
```
### Fish:
Redefine `fish_prompt` in `~/.config/fish/config.fish`:
```fish
        function fish_prompt
            /home/kkszysiu/powerline-shell-go/dist/powerline-shell-go $status --shell bare ^/dev/null
        end
```

And that's all.

Before step 8 you should type: `./dist/powerline-shell-go`, if output is weird and You see a lot of weird characters - that's good. They will be converted to colors and symbols by `bash`.

Please also remember that you have to change your terminal font to one of `https://github.com/Lokaltog/powerline-fonts`.