package segment

import (
	"os"
	"strings"

	"fmt"

	"bitbucket.org/devsamurais/powerline-shell-go/types"
)

var ellipsis = "\u2026"

// CwdSegment is
type CwdSegment struct {
	structs.Segment
}

func (segment *CwdSegment) replaceHomeDir(cwd string) string {
	home := os.Getenv("HOME")

	if strings.HasPrefix(cwd, home) {
		return "~" + strings.TrimPrefix(cwd, home)
	}

	return cwd
}

func (segment *CwdSegment) splitPathIntoNames(cwd string) []string {
	names := strings.Split(cwd, string(os.PathSeparator))

	if names[0] == "" {
		names = names[1:]
	}

	if names[0] == "" {
		return []string{"/"}
	}

	return names
}

// Returns true if the given directory name matches the home indicator and
// the chosen theme should use a special home indicator display.
func (segment *CwdSegment) requiresSpecialHomeDisplay(name string, powerline *structs.Powerline) bool {
	return (name == "~" && powerline.Colors.HomeSpecialDisplay)
}

// If the user has asked for each directory name to be shortened, will
// return the name up to their specified length. Otherwise returns the full
// name.
func (segment *CwdSegment) maybeShortenName(name string, powerline *structs.Powerline) string {
	if powerline.Args.CwdMaxDirSize > 0 {
		return name[:powerline.Args.CwdMaxDirSize]
	}

	return name
}

// Returns the foreground and background color to use for the given name.
func (segment *CwdSegment) getFgBg(name string, powerline *structs.Powerline) (int, int) {
	if segment.requiresSpecialHomeDisplay(name, powerline) {
		return powerline.Colors.HomeFg, powerline.Colors.HomeBg
	}
	return powerline.Colors.PathFg, powerline.Colors.PathBg
}

// Add adds segment to powerline
func (segment *CwdSegment) Add(powerline *structs.Powerline) {
	cwd := powerline.Cwd

	cwd = segment.replaceHomeDir(cwd)

	if powerline.Args.CwdMode == "plain" {
		powerline.Append(structs.PowerlineAppendArgs{Content: fmt.Sprintf(" %s ", cwd), Fg: powerline.Colors.CwdFg, Bg: powerline.Colors.PathBg})
		return
	}

	names := segment.splitPathIntoNames(cwd)
	maxDepth := powerline.Args.CwdMaxDepth
	if maxDepth <= 0 {
		panic("Ignoring --cwd-max-depth argument since it's not greater than 0")
	} else if len(names) > maxDepth {
		// https://github.com/milkbikis/powerline-shell/issues/148
		// n_before is the number is the number of directories to put before the
		// ellipsis. So if you are at ~/a/b/c/d/e and max depth is 4, it will
		// show `~ a ... d e`.
		//
		// maxDepth must be greater than n_before or else you end up repeating
		// parts of the path with the way the splicing is written below.
		nBefore := maxDepth - 1
		if maxDepth > 2 {
			nBefore = 2
		}

		names2 := names

		names = names[:nBefore]
		names = append(names, ellipsis)
		names = append(names, names2[maxDepth:len(names2)]...)
	}

	if powerline.Args.CwdMode == "dironly" {
		// The user has indicated they only want the current directory to be
		// displayed, so chop everything else off

		//names = names[-1:]
		names = []string{names[len(names)-1]}
	}

	for i, name := range names {
		fg, bg := segment.getFgBg(name, powerline)

		separator := powerline.SeparatorThin
		separatorFg := powerline.Colors.SeparatorFg
		isLastDir := (i == len(names)-1)

		if segment.requiresSpecialHomeDisplay(name, powerline) || isLastDir {
			separator = ""
			separatorFg = -1
		}

		powerline.Append(structs.PowerlineAppendArgs{Content: fmt.Sprintf(" %s ", segment.maybeShortenName(name, powerline)), Fg: fg, Bg: bg, Separator: separator, SeparatorFg: separatorFg})
	}
}
