package structs

// PowerlineColors is a struct keeping all colors information
type PowerlineColors struct {
	UsernameFg     int
	UsernameBg     int
	UsernameRootBg int

	HostnameFg int
	HostnameBg int

	HomeSpecialDisplay bool
	HomeBg             int
	HomeFg             int
	PathBg             int
	PathFg             int

	CwdFg       int
	SeparatorFg int

	ReadonlyBg int
	ReadonlyFg int

	SshBg int
	SshFg int

	RepoCleanBg int
	RepoCleanFg int
	RepoDirtyBg int
	RepoDirtyFg int

	JobsFg int
	JobsBg int

	CmdPassedBg int
	CmdPassedFg int
	CmdFailedBg int
	CmdFailedFg int

	SvnChangesBg int
	SvnChangesFg int

	GitAheadBg      int
	GitAheadFg      int
	GitBehindBg     int
	GitBehindFg     int
	GitStagedBg     int
	GitStagedFg     int
	GitNonStagedBg  int
	GitNonStagedFg  int
	GitUntrackedBg  int
	GitUntrackedFg  int
	GitConflictedBg int
	GitConflictedFg int

	VirtualEnvBg int
	VirtualEnvFg int
}

// Theme keeps information about theme
type Theme interface {
	GetColors() PowerlineColors
}

// Segment keeps information about segment
type Segment interface {
	Add()
}

// GitStatus is a type holding info about status of git repo
type GitStatus struct {
	HasPendingCommits bool
	HasUntrackedFiles bool
	OriginPosition    string
}

// Symbols is a type holding info about symbols
type Symbols struct {
	Lock          string
	Network       string
	Separator     string
	SeparatorThin string
}

// PowerlineAppendArgs is
type PowerlineAppendArgs struct {
	Content     string
	Fg          int
	Bg          int
	Separator   string
	SeparatorFg int
}

// PowerlineArgs keeps information about passed arguments from command line
type PowerlineArgs struct {
	ColorizeHostname bool
	CwdMaxDepth      int
	CwdMaxDirSize    int
	CwdMode          string
	Mode             string
	PrevError        int
	Shell            string
}
