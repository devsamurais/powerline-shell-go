package structs

import (
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"path"
	"strings"
)

var colorTemplates = map[string]string{
	"bash": "\\[\\e%s\\]",
	"zsh": "%%{%s%%}",
	"bare": "%s",
}

var symbols = map[string]Symbols{
	"compatible": Symbols{
		Lock:          "RO",
		Network:       "SSH",
		Separator:     "\u25B6",
		SeparatorThin: "\u276F",
	},
	"patched": Symbols{
		Lock:          "\uE0A2",
		Network:       "\uE0A2",
		Separator:     "\uE0B0",
		SeparatorThin: "\uE0B1",
	},
	"flat": Symbols{
		Lock:          "",
		Network:       "",
		Separator:     "",
		SeparatorThin: "",
	},
}

// Powerline is a main structure keeping all important info about an app
type Powerline struct {
	Args          PowerlineArgs
	Cwd           string
	ColorTemplate string
	Reset         string
	Lock          string
	Network       string
	Separator     string
	SeparatorThin string
	Colors        PowerlineColors
	Segments      []PowerlineAppendArgs
}

// SetColorTemplate sets color template
func (p *Powerline) SetColorTemplate() {
	p.ColorTemplate = colorTemplates[p.Args.Shell]
}

// SetReset sets reset
func (p *Powerline) SetReset() {
	p.Reset = fmt.Sprintf(p.ColorTemplate, "[0m")
}

// SetLock sets locks
func (p *Powerline) SetLock() {
	p.Lock = symbols[p.Args.Mode].Lock
}

// SetNetwork sets network
func (p *Powerline) SetNetwork() {
	p.Network = symbols[p.Args.Mode].Network
}

// SetSeparator sets separator
func (p *Powerline) SetSeparator() {
	p.Separator = symbols[p.Args.Mode].Separator
}

// SetSeparatorThin is setting separator thin
func (p *Powerline) SetSeparatorThin() {
	p.SeparatorThin = symbols[p.Args.Mode].SeparatorThin
}

// Color sets color
func (p Powerline) Color(prefix string, code int) string {
	return fmt.Sprintf(p.ColorTemplate, fmt.Sprintf("[%s;5;%dm", prefix, code))
}

// FGColor sets foreground color
func (p Powerline) FGColor(code int) string {
	return p.Color("38", code)
}

// BGColor sets background color
func (p Powerline) BGColor(code int) string {
	return p.Color("48", code)
}

// Append appends segments
func (p *Powerline) Append(args PowerlineAppendArgs) {
	if args.Separator == "" {
		args.Separator = p.Separator
	}

	if args.SeparatorFg == 0 || args.SeparatorFg == -1 {
		args.SeparatorFg = args.Bg
	}

	p.Segments = append(p.Segments, args)
}

// Draw is drawing segments
func (p Powerline) Draw() string {
	out := ""

	for idx := range p.Segments {
		out += p.DrawSegment(idx)
	}
	out += p.Reset
	return out
}

// DrawSegment draws segment
func (p Powerline) DrawSegment(idx int) string {
	out := ""
	var segment = p.Segments[idx]
	var nextSegment PowerlineAppendArgs
	if idx < len(p.Segments)-1 {
		nextSegment = p.Segments[idx+1]
	}

	out += p.FGColor(segment.Fg)
	out += p.BGColor(segment.Bg)
	out += segment.Content
	if nextSegment.Content != "" {
		out += p.BGColor(nextSegment.Bg)
	} else {
		out += p.Reset
	}
	out += p.FGColor(segment.SeparatorFg)
	out += segment.Separator
	//     segment = p.segments[idx]
	//     next_segment = self.segments[idx + 1] if idx < len(self.segments)-1 else None
	//
	//         return ''.join((
	//             self.fgcolor(segment[1]),
	//             self.bgcolor(segment[2]),
	//             segment[0],
	//             self.bgcolor(next_segment[2]) if next_segment else self.reset,
	//             self.fgcolor(segment[4]),
	//             segment[3]))
	return out
}

func (p *Powerline) AddVirtualEnvSegment() {
	env := os.Getenv("VIRTUAL_ENV")
	if env == "" {
		return
	}

	envName := path.Base(env)
	content := fmt.Sprintf(" %s ", envName)

	p.Append(PowerlineAppendArgs{Content: content, Fg: p.Colors.VirtualEnvFg, Bg: p.Colors.VirtualEnvBg})
}

func (p *Powerline) AddUsernameSegment() {
	userPrompt := ""
	bgcolor := 0

	if p.Args.Shell == "bash" {
		userPrompt = " \\u "
	} else if p.Args.Shell == "zsh" {
		userPrompt = " %n "
	} else {
		userPrompt = fmt.Sprintf(" %s ", os.Getenv("USER"))
	}

	if os.Getenv("USER") == "root" {
		bgcolor = p.Colors.UsernameRootBg
	} else {
		bgcolor = p.Colors.UsernameBg
	}

	p.Append(PowerlineAppendArgs{Content: userPrompt, Fg: p.Colors.UsernameFg, Bg: bgcolor})
}

func (p *Powerline) AddHostnameSegment() {
	hostPrompt := ""
	hostname, err := os.Hostname()
	if err != nil {
		panic(err)
	}

	if p.Args.ColorizeHostname {

		//from lib.color_compliment import stringToHashToColorAndOpposite
		//from lib.colortrans import rgb2short
		//FG, BG = stringToHashToColorAndOpposite(hostname)
		//FG, BG = (rgb2short(*color) for color in [FG, BG])
		//host_prompt = ' %s' % hostname.split('.')[0]

		//powerline.append(host_prompt, FG, BG)
	} else {
		if p.Args.Shell == "bash" {
			hostPrompt = " \\h "
		} else if p.Args.Shell == "zsh" {
			hostPrompt = " %m "
		} else {
			hostPrompt = fmt.Sprintf(" %s ", strings.Split(hostname, ".")[0])
		}

		p.Append(PowerlineAppendArgs{Content: hostPrompt, Fg: p.Colors.HostnameFg, Bg: p.Colors.HostnameBg})
	}
}

// AddSSHSegment adds SSH segment
func (p *Powerline) AddSSHSegment() {
	sshClient := os.Getenv("SSH_CLIENT")
	content := fmt.Sprintf(" %s ", p.Network)
	if sshClient != "" {
		p.Append(PowerlineAppendArgs{Content: content, Fg: p.Colors.SshFg, Bg: p.Colors.SshBg})
	}
}

// GetShortPath returns short path
func GetShortPath(cwd string) []string {
	home := os.Getenv("HOME")
	homeDir, err := os.Stat(home)
	if err != nil {
		panic(err)
	}

	names := strings.Split(cwd, string(os.PathSeparator))

	if names[0] == "" {
		names = names[1:]
	}

	path := ""
	for index := 0; index < len(names); index++ {
		path += string(os.PathSeparator) + names[index]

		pathDir, err := os.Stat(path)
		if err != nil {
			panic(err)
		}

		if os.SameFile(pathDir, homeDir) {
			var ind = index + 1
			return append([]string{"~"}, names[ind:]...)
		}
	}

	if len(names) == 0 {
		return []string{"~"}
	}
	return names
}

// AddReadOnlySegment adds read only segment
func (p *Powerline) AddReadOnlySegment() {
	//     var cwd string
	//     if p.cwd != "" {
	//         cwd = p.cwd
	//     } else {
	//         cwd = os.Getenv("PWD")
	//     }

	// TODO: fix that
	//if not os.access(cwd, os.W_OK):
	//    powerline.append(' %s ' % powerline.lock, Color.READONLY_FG, Color.READONLY_BG)
}

func GetGitStatus() GitStatus {
	var gitstatus GitStatus
	gitstatus.HasPendingCommits = true
	gitstatus.HasUntrackedFiles = false

	cmd := exec.Command("git", "status", "--ignore-submodules")

	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		panic(err)
	}

	for _, line := range strings.Split(out.String(), "\n") {
		occurence := strings.Index(line, "Your branch is ")
		if occurence != -1 {
			selectedLine := line[occurence+len("Your branch is "):]
			parts := strings.Split(selectedLine, " ")

			byOccurence := strings.Index(selectedLine, " by ")
			commitOccurence := strings.Index(selectedLine, " commit")

			if commitOccurence != -1 {
				commitsCnt := selectedLine[byOccurence+len(" by ") : commitOccurence]
				gitstatus.OriginPosition += fmt.Sprintf(" %s", strings.TrimSpace(commitsCnt))

				if parts[0] == "behind" {
					gitstatus.OriginPosition += "\u21E3"
				} else if parts[0] == "ahead" {
					gitstatus.OriginPosition += "\u21E1"
				}
			}
		}

		if strings.Contains(line, "nothing to commit") {
			gitstatus.HasPendingCommits = false
		}
		if strings.Contains(line, "Untracked files") {
			gitstatus.HasUntrackedFiles = true
		}
	}

	return gitstatus
}

func (p *Powerline) AddGitSegment() {
	// git branch 2> /dev/null | grep -e '\\*'
	cmdGitBranch := exec.Command("git", "branch", "--no-color")
	var gitBranchOut bytes.Buffer
	cmdGitBranch.Stdout = &gitBranchOut
	err := cmdGitBranch.Run()
	if err != nil {
		//panic(err)
		return
	}

	cmdGrep := exec.Command("grep", "-e", "\\*")
	cmdGrep.Stdin = &gitBranchOut

	var grepOut bytes.Buffer
	cmdGrep.Stdout = &grepOut
	err2 := cmdGrep.Run()
	if err2 != nil {
		// show (no branch) segment in that case
		content := fmt.Sprintf(" %s ", "(no branch)")

		bg := p.Colors.RepoCleanBg
		fg := p.Colors.RepoCleanFg
		p.Append(PowerlineAppendArgs{Content: content, Fg: fg, Bg: bg})
		//panic(err2)
		//return
	}

	if grepOut.String() == "" {
		return
	}

	branch := strings.TrimSpace(grepOut.String()[2:])

	gitstatus := GetGitStatus()

	branch += gitstatus.OriginPosition
	if gitstatus.HasUntrackedFiles {
		branch += " +"
	}

	bg := p.Colors.RepoCleanBg
	fg := p.Colors.RepoCleanFg
	if gitstatus.HasPendingCommits {
		bg = p.Colors.RepoDirtyBg
		fg = p.Colors.RepoDirtyFg
	}

	content := fmt.Sprintf(" %s ", branch)

	p.Append(PowerlineAppendArgs{Content: content, Fg: fg, Bg: bg})
}

func (p *Powerline) AddRootIndicatorSegment() {
	rootIndicators := map[string]string{
		"bash": " \\$ ",
		"zsh":  " \\$ ",
		"bare": " $ ",
	}
	bg := p.Colors.CmdPassedBg
	fg := p.Colors.CmdPassedFg
	if p.Args.PrevError != 0 {
		fg = p.Colors.CmdFailedFg
		bg = p.Colors.CmdPassedBg
	}

	p.Append(PowerlineAppendArgs{Content: rootIndicators[p.Args.Shell], Fg: fg, Bg: bg})
}
