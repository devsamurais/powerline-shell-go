package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/dombenson/go-ini"

	segment "bitbucket.org/devsamurais/powerline-shell-go/segments"
	"bitbucket.org/devsamurais/powerline-shell-go/themes"
	"bitbucket.org/devsamurais/powerline-shell-go/types"
)

var themes = map[string]structs.Theme{
	"basic":          theme.Basic{},
	"default":        theme.DefaultTheme{},
	"solarized_dark": theme.SolarizedDark{},
	"washed":         theme.Washed{},
}

var shell = "bash"
var mode = "patched"
var cwdmode = "fancy"
var cwdmaxdepth = 5
var colors = structs.PowerlineColors{}
var segments = map[string]struct{}{}

// GetValidCwd check if the current working directory is valid or not.
func GetValidCwd() string {
	//    We check if the current working directory is valid or not. Typically
	//    happens when you checkout a different branch on git that doesn't have
	//    this directory.
	//    We return the original cwd because the shell still considers that to be
	//    the working directory, so returning our guess will confuse people
	//
	wd, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	// TODO: write error handling here...
	//     try:
	//         cwd = os.getcwd()
	//     except:
	//         cwd = os.getenv('PWD')  # This is where the OS thinks we are
	//         parts = cwd.split(os.sep)
	//         up = cwd
	//         while parts and not os.path.exists(up):
	//             parts.pop()
	//             up = os.sep.join(parts)
	//         try:
	//             os.chdir(up)
	//         except:
	//             warn("Your current directory is invalid.")
	//             sys.exit(1)
	//         warn("Your current directory is invalid. Lowest valid directory: " + up)
	//     return cwd
	return wd
}

// LoadConfigAndTheme is loading config file and sets choosen segments and theme
func LoadConfigAndTheme(absExecPath string) {
	configPath := fmt.Sprintf("%s/config.ini", filepath.Dir(absExecPath))
	configfile, err := ini.LoadFile(configPath)
	if err != nil {
		panic(err)
	}

	segmentsStr, ok := configfile.GetArr("default", "segments")
	if !ok {
		panic(fmt.Sprintf("'segments' variable missing from config (%s)", configPath))
	}

	for _, s := range segmentsStr {
		segments[s] = struct{}{}
	}

	theme, ok := configfile.Get("default", "theme")
	if !ok {
		panic("'theme' variable missing from config")
	}

	shell, ok = configfile.Get("default", "shell")
	if !ok {
		panic("'shell' variable missing from config")
	}

	mode, ok = configfile.Get("default", "mode")
	if !ok {
		panic("'mode' variable missing from config")
	}

	cwdmode, ok = configfile.Get("default", "cwdmode")
	if !ok {
		panic("'cwdmode' variable missing from config")
	}

	cwdmaxdepth, ok = configfile.GetInt("default", "cwdmaxdepth")
	if !ok {
		panic("'cwdmaxdepth' variable missing from config")
	}

	if themes[theme] == nil {
		keys := make([]string, 0, len(themes))
		for k := range themes {
			keys = append(keys, k)
		}

		fmt.Println(fmt.Sprintf("Selected theme not found. Available themes: %s", strings.Join(keys, ",")))
		os.Exit(1)
	}

	colors = themes[theme].GetColors()
}

func isSegmentEnabled(name string) bool {
	_, enabled := segments[name]

	return enabled
}

func main() {
	execPath := os.Args[0]
	absExecPath, err := filepath.Abs(execPath)
	if err != nil {
		panic(err)
	}

	var prevError int
	if len(os.Args) > 1 {
		prevError, err = strconv.Atoi(os.Args[1])
		if err != nil {
			panic(err)
		}
	}

	LoadConfigAndTheme(absExecPath)

	// colorize_hostname=False, cwd_max_depth=5, cwd_only=False, mode='patched', prev_error=0, shell='bash'
	args := structs.PowerlineArgs{ColorizeHostname: false, CwdMaxDepth: cwdmaxdepth, CwdMode: cwdmode, Mode: mode, PrevError: prevError, Shell: shell}

	p := structs.Powerline{Args: args, Cwd: GetValidCwd(), Colors: colors}
	p.SetColorTemplate()
	p.SetReset()
	p.SetLock()
	p.SetNetwork()
	p.SetSeparator()
	p.SetSeparatorThin()

	p.AddVirtualEnvSegment()
	p.AddUsernameSegment()
	p.AddHostnameSegment()
	p.AddSSHSegment()

	if isSegmentEnabled("cwd") {
		cwdSegment := segment.CwdSegment{}
		cwdSegment.Add(&p)
	}

	p.AddGitSegment()
	p.AddRootIndicatorSegment()

	fmt.Printf(p.Draw())
}
