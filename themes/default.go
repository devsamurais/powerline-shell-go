package theme

import "bitbucket.org/devsamurais/powerline-shell-go/types"

// DefaultTheme struct should have the default colors for every segment.
// Please test every new segment with this theme first.
type DefaultTheme struct {
	structs.Theme
}

func (t DefaultTheme) GetColors() structs.PowerlineColors {
	colors := structs.PowerlineColors{}
	colors.UsernameFg = 250
	colors.UsernameBg = 240
	colors.UsernameRootBg = 124

	colors.HostnameFg = 250
	colors.HostnameBg = 238

	colors.HomeSpecialDisplay = true
	colors.HomeBg = 31  // blueish
	colors.HomeFg = 15  // white
	colors.PathBg = 237 // dark grey
	colors.PathFg = 250 // light grey
	colors.CwdFg = 254  // nearly-white grey
	colors.SeparatorFg = 244

	colors.ReadonlyBg = 124
	colors.ReadonlyFg = 254

	colors.SshBg = 166 // medium orange
	colors.SshFg = 254

	colors.RepoCleanBg = 148 // a light green color
	colors.RepoCleanFg = 0   // black
	colors.RepoDirtyBg = 161 // pink/red
	colors.RepoDirtyFg = 15  // white

	colors.JobsFg = 39
	colors.JobsBg = 238

	colors.CmdPassedBg = 236
	colors.CmdPassedFg = 15
	colors.CmdFailedBg = 161
	colors.CmdFailedFg = 15

	colors.SvnChangesBg = 148
	colors.SvnChangesFg = 22 // dark green

	colors.GitAheadBg = 240
	colors.GitAheadFg = 250
	colors.GitBehindBg = 240
	colors.GitBehindFg = 250
	colors.GitStagedBg = 22
	colors.GitStagedFg = 15
	colors.GitNonStagedBg = 130
	colors.GitNonStagedFg = 15
	colors.GitUntrackedBg = 52
	colors.GitUntrackedFg = 15
	colors.GitConflictedBg = 9
	colors.GitConflictedFg = 15

	colors.VirtualEnvBg = 35 // a mid-tone green
	colors.VirtualEnvFg = 00

	return colors
}
