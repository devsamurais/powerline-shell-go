package theme

import "bitbucket.org/devsamurais/powerline-shell-go/types"

// Basic theme which only uses colors in 0-15 range
type Basic struct {
	structs.Theme
}

func (t Basic) GetColors() structs.PowerlineColors {
	dt := DefaultTheme{}
	colors := dt.GetColors()

	colors.UsernameFg = 8
	colors.UsernameBg = 15
	colors.UsernameRootBg = 1

	colors.HostnameFg = 8
	colors.HostnameBg = 7

	colors.HomeSpecialDisplay = false
	colors.PathBg = 8 // dark grey
	colors.PathFg = 7 // light grey
	colors.CwdFg = 15 // white
	colors.SeparatorFg = 7

	colors.ReadonlyBg = 1
	colors.ReadonlyFg = 15

	colors.RepoCleanBg = 2  // green
	colors.RepoCleanFg = 0  // black
	colors.RepoDirtyBg = 1  // red
	colors.RepoDirtyFg = 15 // white

	colors.JobsFg = 14
	colors.JobsBg = 8

	colors.CmdPassedBg = 8
	colors.CmdPassedFg = 15
	colors.CmdFailedBg = 11
	colors.CmdFailedFg = 0

	colors.SvnChangesBg = colors.RepoDirtyBg
	colors.SvnChangesFg = colors.RepoDirtyFg

	colors.VirtualEnvBg = 2
	colors.VirtualEnvFg = 0

	return colors
}
