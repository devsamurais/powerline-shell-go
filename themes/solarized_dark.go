package theme

import "bitbucket.org/devsamurais/powerline-shell-go/types"

// SolarizedDark is a darky theme
type SolarizedDark struct {
	structs.Theme
}

func (t SolarizedDark) GetColors() structs.PowerlineColors {
	dt := DefaultTheme{}
	colors := dt.GetColors()

	colors.UsernameFg = 15
	colors.UsernameBg = 4
	colors.UsernameRootBg = 1

	colors.HostnameFg = 15
	colors.HostnameFg = 10

	colors.HomeSpecialDisplay = false
	colors.PathFg = 7
	colors.PathBg = 10
	colors.CwdFg = 15
	colors.SeparatorFg = 14

	colors.ReadonlyBg = 1
	colors.ReadonlyFg = 7

	colors.RepoCleanFg = 14
	colors.RepoCleanBg = 0
	colors.RepoDirtyFg = 3
	colors.RepoDirtyBg = 0

	colors.JobsFg = 4
	colors.JobsBg = 8

	colors.CmdPassedFg = 15
	colors.CmdPassedBg = 2
	colors.CmdFailedFg = 15
	colors.CmdFailedBg = 1

	colors.SvnChangesFg = colors.RepoDirtyFg
	colors.SvnChangesBg = colors.RepoDirtyBg

	colors.VirtualEnvBg = 15
	colors.VirtualEnvFg = 2

	return colors
}
