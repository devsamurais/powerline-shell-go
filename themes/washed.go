package theme

import "bitbucket.org/devsamurais/powerline-shell-go/types"

// Basic theme which only uses colors in 0-15 range
type Washed struct {
	structs.Theme
}

func (t Washed) GetColors() structs.PowerlineColors {
	dt := DefaultTheme{}
	colors := dt.GetColors()

	colors.UsernameFg = 8
	colors.UsernameBg = 251
	colors.UsernameRootBg = 209

	colors.HostnameFg = 8
	colors.HostnameBg = 7

	colors.HomeSpecialDisplay = false
	colors.PathBg = 15
	colors.PathFg = 8
	colors.CwdFg = 8
	colors.SeparatorFg = 251

	colors.ReadonlyBg = 209
	colors.ReadonlyFg = 15

	colors.RepoCleanBg = 150 // pale green
	colors.RepoCleanFg = 235
	colors.RepoDirtyBg = 203 // pale red
	colors.RepoDirtyFg = 15

	colors.JobsFg = 14
	colors.JobsBg = 8

	colors.CmdPassedFg = 8
	colors.CmdPassedBg = 7
	colors.CmdFailedFg = 15
	colors.CmdFailedBg = 9

	colors.SvnChangesFg = colors.RepoDirtyFg
	colors.SvnChangesBg = colors.RepoDirtyBg

	colors.VirtualEnvBg = 150
	colors.VirtualEnvFg = 0

	return colors
}
